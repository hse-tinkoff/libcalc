# libcalc

Model sample of library with calculator implementation

## Advanced project structure

```
.
├── CMakeLists.txt
├── include
│   ├── aaa.h
│   ├── bbb.h
│   ├── ...
│   ├── yyy.h
│   └── zzz.h
├── src
│   ├── aaa.cpp
│   ├── bbb.cpp
│   ├── ...
│   ├── yyy.cpp
│   └── zzz.cpp
├── tests
│   └── test*.cpp
└── libs
     ├── A
     |   ├── CMakeLists.txt
     │   ├── include
     │   │   └── A
     │   │       ├── A.h
     │   │       └── ...
     │   ├── src
     │   │   ├── A.cpp
     │   │   └── ...
     │   └── test
     │       └── ...
     └── B
         ├── CMakeLists.txt
         ├── include
         │   └── B
         │       ├── B.h
         │       └── ...
         ├── src
         │   ├── B.cpp
         │   └── ...
         └── test
             └── ...
```
